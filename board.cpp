#include "board.hpp"

bool board::if_won(int length, int &winner)
{
    int loopa, loopb, count, tempa, tempb;
    count = 0;
    for (int i = 0; i < size; i++)
    {
        for (int j = 0; j < size; j++)
        {
            if (arr[i][j] != '0')
            {
                loopa = i;
                loopb = j;
                while (arr[loopa][loopb] == arr[i][j]) // sprawdzam rzad w prawo
                {
                    tempa = loopb;
                    if (tempa++ >= (size))
                    {
                        break;
                    }
                    if (arr[i][j] == arr[loopa][loopb++])
                    {
                        count++;
                        if (count == length)
                        {
                            if (arr[i][j] == 'x')
                            {
                                winner = 1;
                            }
                            else
                            {
                                winner = 2;
                            }
                            return true;
                        }
                    }
                    tempa = loopb;
                    if (tempa++ >= (size))
                    {
                        break;
                    }
                }
                count = 0;

                loopa = i;
                loopb = j;
                if (i > 0)
                {
                    while (arr[loopa][loopb] == arr[i][j]) // sprawdzam po skosie w prawo i w gore
                    {
                        if (loopa < 0 && loopb >= size)
                        {
                            break;
                        }

                        if (arr[i][j] == arr[loopa][loopb++])
                        {
                            count++;
                            if (count == length)
                            {
                                if (arr[i][j] == 'x')
                                {
                                    winner = 1;
                                }
                                else
                                {
                                    winner = 2;
                                }
                                return true;
                            }
                            if (loopa != 0)
                                loopa--;
                        }
                        if (loopa < 0 && loopb >= size)
                        {
                            break;
                        }
                    }
                }
                count = 0;

                loopa = i;
                loopb = j;
                if (i < (size - 1))
                {
                    while (arr[loopa][loopb] == arr[i][j]) // sprawdzam po skosie w prawo i w dol
                    {
                        if (arr[i][j] == arr[loopa][loopb])
                        {
                            count++;
                            if (count == length)
                            {
                                if (arr[i][j] == 'x')
                                {
                                    winner = 1;
                                }
                                else
                                {
                                    winner = 2;
                                }
                                return true;
                            }
                            if (loopa <= size && loopb <= size)
                            {
                                loopa++;
                                loopb++;
                            }
                            else
                                break;
                        }
                        if (loopa >= size && loopb >= size)
                        {
                            break;
                        }
                    }
                }
                count = 0;

                loopa = i;
                loopb = j;
                if (i < size)
                {
                    while (arr[loopa][loopb] == arr[i][j]) // sprawdzam rzad w dol
                    {
                        if (loopa >= size)
                        {
                            break;
                        }
                        if (arr[i][j] == arr[loopa][loopb])
                        {
                            count++;
                            if (count == length)
                            {
                                if (arr[i][j] == 'x')
                                {
                                    winner = 1;
                                }
                                else
                                {
                                    winner = 2;
                                }
                                return true;
                            }
                            loopa++;
                        }
                        if (loopa >= size)
                        {
                            break;
                        }
                    }
                    count = 0;
                }
            }
        }
    }
    return false;
}

void board::num()
{
    std::cout << "Numeracja pol na planszy:" << std::endl;
    int count = 0;
    for (int i = 0; i < size; i++)
    {
        for (int j = 0; j < size; j++)
        {
            if (j == (size - 1))
            {
                std::cout << " " << ++count << " ";
            }
            else
            {
                std::cout << " " << ++count << " |";
            }
        }
        std::cout << std::endl;
        if (i != (size - 1))
        {
            for (int k = 0; k < size; k++)
            {
                std::cout << "----";
            }
        }
        std::cout << std::endl;
    }
}

void board::print()
{
    for (int i = 0; i < size; i++)
    {
        for (int j = 0; j < size; j++)
        {
            if (j == (size - 1))
            {
                if (arr[i][j] != '0')
                    std::cout << " " << arr[i][j] << " ";
                else
                    std::cout << "   ";
            }
            else
            {
                if (arr[i][j] != '0')
                    std::cout << " " << arr[i][j] << " |";
                else
                    std::cout << "   |";
            }
        }
        std::cout << std::endl;
        if (i != (size - 1))
        {
            for (int k = 0; k < size; k++)
            {
                std::cout << "----";
            }
        }
        std::cout << std::endl;
    }
}

int board::turn(int y, int x, int player)
{
    if (arr[x][y] != '0')
    {
        std::cout << "Pole już zajęte, wybierz ponownie" << std::endl;
        return 0;
    }
    else if (x > size || y > size)
    {
        std::cout << "Pole poza planszą, wybierz ponownie" << std::endl;
        return 0;
    }
    else
    {
        if (player == 1)
        {
            arr[x][y] = 'x';
        }
        else
        {
            arr[x][y] = 'o';
        }
    }
    return 1;
}

bool board::end(int length)
{
    int winner;

    for (int i = 0; i < size; i++)
    {
        for (int j = 0; j < size; j++)
        {
            if (if_won(length, winner) == 1)
            {
                if (winner == 1)
                {
                    std::cout << "Wygral gracz 1! (x)" << std::endl;
                    return true;
                }
                if (winner == 2)
                {
                    std::cout << "Wygral gracz 2! (o)" << std::endl;
                    return true;
                }
            }
        }
    }

    for (int i = 0; i < size; i++)
    {
        for (int j = 0; j < size; j++)
        {
            if (arr[i][j] == '0') // check if board full
            {
                return false;
            }
        }
    }
    std::cout << "plansza zapelniona. Wynik gry: Remis" << std::endl;
    return true;
}
