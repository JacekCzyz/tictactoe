#include <iostream>

class board
{
private:
    char **arr;
    int size;

public:
    board() {}
    board(int width)
    {
        size = width;
        arr = new char *[size];
        for (int i = 0; i < size; i++)
        {
            arr[i] = new char[size];
        }

        for (int i = 0; i < size; i++)
        {
            for (int j = 0; j < size; j++)
            {
                arr[i][j] = '0';
            }
        }
    }

    ~board(){};
    void operator=(board nboard)
    {
        size = nboard.ret_size();
        arr = new char *[size];
        for (int i = 0; i < size; i++)
        {
            arr[i] = new char[size];
        }

        for (int i = 0; i < size; i++)
        {
            for (int j = 0; j < size; j++)
            {
                arr[i][j] = nboard.ret_pos(i, j);
            }
        }
    }
    char** ret_arr(){return arr;}
    void num();
    void print();
    int turn(int y, int x, int player);
    bool end(int length);
    bool if_won(int length, int &winner);
    int ret_size() { return size; }
    char ret_pos(int i, int j) { return arr[i][j]; }
};