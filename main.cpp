#include "mighty_op.cpp"

using namespace std;
int main()
{
    int size, spot, x, y, player, length;
    while (1)
    {
        cout << "Podaj rozmiar planszy(minimum 3): ";
        cin >> size;
        if (size > 2)
        {
            while (1)
            {
                cout << "Podaj liczbe znakow w rzedzie by wygrac: ";
                cin >> length;
                if (length <= size)
                {
                    break;
                }
                cout << "Niepoprawna ilosc znakow. Powinna byc mniejsza/rowna rozmiarowi planszy" << endl;
            }
            break;
        }
        else
            cout << "Niepoprawny wymiar, ma wynosic minimum 3" << endl;
    }
    board game(size);
    tree opponent;

    cout << endl
         << "Aktualny stan planszy" << endl;
    game.print();

    player = 1;
    while (game.end(length) == false)
    {

        if (player == 1)
        {
            game.num();
            cout << "Numer pola w ktorym umiescic znak: ";
            cin >> spot;

            int x = (spot - 1) % size;
            int y = (spot - 1) / size;

            while (game.turn(x, y, player) == 0)
            {
                cin >> spot;
                x = (spot - 1) % size;
                y = (spot - 1) / size;
            }
        }
        else
        {
            opponent.ntree(game, length);
        }

        if (player == 1)
            player = 2;
        else
            player = 1;
        game.print();
    }
    return 0;
}