#include "mighty_op.hpp"
// #include "helpers.cpp"

void check_round(board move, int &value, int i, int j)
{

    if ((i + 1) < move.ret_size())
    {
        if (move.ret_pos(i + 1, j) == 'o')
            value += 1;

        if ((j + 1) < move.ret_size())
        {
            if (move.ret_pos(i + 1, j + 1) == 'o')
                value += 1;
        }
    }
    if ((i - 1) >= 0)
    {
        if (move.ret_pos(i - 1, j) == 'o')
            value += 1;

        if ((j + 1) < move.ret_size())
        {
            if (move.ret_pos(i - 1, j + 1) == 'o')
                value += 1;
        }
    }
    if ((j - 1) >= 0)
    {
        if (move.ret_pos(i, j - 1) == 'o')
            value += 1;

        if ((i + 1) < move.ret_size())
        {
            if (move.ret_pos(i + 1, j - 1) == 'o')
                value += 1;
        }
    }
    if ((j + 1) < move.ret_size())
    {
        if (move.ret_pos(i, j + 1) == 'o')
            value += 1;

        if ((i - 1) >= 0)
        {
            if (move.ret_pos(i - 1, j - 1) == 'o')
                value += 1;
        }
    }
}

void check_xox(board move, int &value, int i, int j)
{
    if ((i + 1) < move.ret_size())
    {
        if (move.ret_pos(i, j) == 'x')
        {
            if ((i + 1) < move.ret_size())
            {
                if (move.ret_pos(i + 1, j) == 'o')
                {
                    if ((i + 2) < move.ret_size())
                    {
                        if (move.ret_pos(i + 2, j) == 'x')
                        {
                            value += 3;
                        }
                    }
                }
            }
        }

        if ((j + 1) < move.ret_size())
        {
            if (move.ret_pos(i, j) == 'x')
            {
                if ((i + 1) < move.ret_size() && (j + 1) <= move.ret_size())
                {
                    if (move.ret_pos(i + 1, j + 1) == 'o')
                    {
                        if ((i + 2) < move.ret_size() && (j + 2) <= move.ret_size())
                        {
                            if (move.ret_pos(i + 2, j + 2) == 'x')
                            {
                                value += 3;
                            }
                        }
                    }
                }
            }
        }
    }

    if ((i - 1) >= 0)
    {
        if (move.ret_pos(i, j) == 'x')
        {
            if (move.ret_pos(i - 1, j) == 'o')
            {
                if ((i - 2) >= 0)
                {
                    if (move.ret_pos(i - 2, j) == 'x')
                    {
                        value += 3;
                    }
                }
            }
        }

        if ((j + 1) < move.ret_size())
        {
            if (move.ret_pos(i, j) == 'x')
            {
                if ((i - 1) >= 0 && (j + 1) < move.ret_size())
                {
                    if (move.ret_pos(i - 1, j + 1) == 'o')
                    {
                        if ((i - 2) >= 0 && (j + 2) < move.ret_size())
                        {
                            if (move.ret_pos(i - 2, j + 2) == 'x')
                            {
                                value += 3;
                            }
                        }
                    }
                }
            }
        }
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void check_xxo(board move, int &value, int i, int j, int width)
{
    for (int i = 0; i < move.ret_size(); i++)
    {
        for (int j = 0; j < move.ret_size(); j++)
        {
            if (move.ret_pos(i, j) == 'x')
            {

                if (j < move.ret_size())
                {
                    if (j + 1 < move.ret_size()) // sprawdzam w rzedzie w prawo
                    {
                        if (move.ret_pos(i, j + 1) == 'x')
                        {
                            if (j + 2 < move.ret_size())
                            {
                                if (move.ret_pos(i, j + 2) == 'o')
                                {
                                    if (j - 1 >= 0)
                                    {
                                        if (move.ret_pos(i, j - 1) == 'o')
                                            value += 15;
                                        else
                                            value += 10;
                                    }
                                    else
                                        value += 15;
                                }
                            }
                        }
                    }
                }

                if (j > 0)
                {
                    if (j - 1 > 0) // sprawdzam w rzedzie w lewo
                    {
                        if (move.ret_pos(i, j - 1) == 'x')
                        {
                            if (j - 2 >= 0)
                            {
                                if (move.ret_pos(i, j - 2) == 'o')
                                {
                                    if (j + 1 < move.ret_size())
                                    {
                                        if (move.ret_pos(i, j + 1) == 'o')
                                            value += 15;
                                        else
                                            value += 10;
                                    }
                                    else
                                        value += 15;
                                }
                            }
                        }
                    }
                }

                if (i > 0 && j < move.ret_size()) // sprawdzam po skosie w prawo i w gore
                {
                    if (j + 1 < move.ret_size() && i - 1 >= 0 && j + 2 < move.ret_size() && i - 2 >= 0)
                    {
                        if (move.ret_pos(i - 1, j + 1) == 'x')
                        {
                            if (move.ret_pos(i - 2, j + 2) == 'o')
                            {
                                if (i + 1 < move.ret_size() || j - 1 >= 0)
                                {
                                    if (move.ret_pos(i + 1, j - 1) == 'o')
                                        value += 15;
                                    else
                                        value += 10;
                                }
                                else
                                    value += 15;
                            }
                        }
                    }
                }

                if (j > 0 && i < move.ret_size()) // sprawdzam po skosie w lewo i w dol
                {
                    if (i + 1 < move.ret_size() && j - 1 >= 0 && i + 2 < move.ret_size() && j - 2 >= 0)
                    {
                        if (move.ret_pos(i + 1, j - 1) == 'x')
                        {
                            if (move.ret_pos(i + 2, j - 2) == 'o')
                            {
                                if (i - 1 >= 0 || j + 1 < move.ret_size())
                                {
                                    if (move.ret_pos(i - 1, j + 1) == 'o')
                                        value += 15;
                                    else
                                        value += 10;
                                }
                                else
                                    value += 15;
                            }
                        }
                    }
                }

                if (i < (move.ret_size() - 1))
                {
                    if (i < (move.ret_size() - 1) && j < (move.ret_size() - 1)) // sprawdzam po skosie w prawo i w dol
                    {
                        if (j + 1 < move.ret_size() && i + 1 < move.ret_size())
                        {
                            if (move.ret_pos(i + 1, j + 1) == 'x')
                            {
                                if (j + 2 < move.ret_size() && i + 2 < move.ret_size())
                                {
                                    if (move.ret_pos(i + 2, j + 2) == 'o')
                                    {
                                        if (i - 1 >= 0 && j - 1 >= 0)
                                        {
                                            if (move.ret_pos(i - 1, j - 1) == 'o')
                                                value += 15;
                                            else
                                                value += 10;
                                        }
                                        else
                                            value += 15;
                                    }
                                }
                            }
                        }
                    }
                }

                if (i < move.ret_size())
                {
                    if (i > 0 && j > 0) // sprawdzam po skosie w lewo i w gore
                    {
                        if (j - 1 >= 0 && i - 1 >= 0)
                        {
                            if (move.ret_pos(i - 1, j - 1) == 'x')
                            {
                                if (j - 2 >= 0 && i - 2 >= 0)
                                {
                                    if (move.ret_pos(i - 2, j - 2) == 'o')
                                    {
                                        if (i + 1 < move.ret_size() && j + 1 < move.ret_size())
                                        {
                                            if (move.ret_pos(i + 1, j + 1) == 'o')
                                                value += 15;
                                            else
                                                value += 10;
                                        }
                                        else
                                            value += 15;
                                    }
                                }
                            }
                        }
                    }
                }

                if (i < (move.ret_size() - 1))
                {
                    if (i + 1 < move.ret_size() && i + 2 < move.ret_size()) // sprawdzam rzad w dol
                    {
                        if (move.ret_pos(i + 1, j) == 'x')
                        {
                            if (move.ret_pos(i + 2, j) == 'o')
                            {
                                if (i - 1 >= 0)
                                {
                                    if (move.ret_pos(i - 1, j) == 'o')
                                        value += 15;
                                    else
                                        value += 10;
                                }
                                else
                                    value += 15;
                            }
                        }
                    }
                }

                if (i > 0)
                {
                    if (i - 1 >= 0 && i - 2 >= 0) // sprawdzam rzad w gore
                    {
                        if (move.ret_pos(i - 1, j) == 'x')
                        {
                            if (move.ret_pos(i - 2, j) == 'o')
                            {
                                if (i + 1 < move.ret_size())
                                {
                                    if (move.ret_pos(i + 1, j) == 'o')
                                        value += 15;
                                    else
                                        value += 10;
                                }
                                else
                                    value += 15;
                            }
                        }
                    }
                }
            }
        }
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void tree_node::mov_board(board cur_board, int pos)
{
    int x = pos % cur_board.ret_size();
    int y = pos / cur_board.ret_size();
    board temp(cur_board.ret_size());
    temp = cur_board;
    temp.turn(x, y, 2);
    move = temp;
}

void tree_node::calc_val(int width)
{
    value = 0;
    int winner = 0;
    int loopa, loopb, countera, counterb;
    if (move.if_won(width, winner) == 1) // chceck if in this board player 2 wins. if yes, value 10000, cannot be higher.
        value = 10000;

    else
    {
        for (int i = 0; i < move.ret_size(); i++)
        {
            for (int j = 0; j < move.ret_size(); j++)
            {
                if (move.ret_pos(i, j) == 'x')
                {
                    check_round(move, value, i, j);      // sprawdzam sasiadujace pola, jesli obok jest o, to daje punkt
                    check_xox(move, value, i, j);        // sprawdzam rzedy znakow, jesli o miedzy 2 x to punkty
                    check_xxo(move, value, i, j, width); // sprawdzam czy zamkniety rzad wiecej niz jeden x
                }
            }
        }
    }
}

void tree::ntree(board &cur_board, int width)
{
    length = width;
    board temp = cur_board;
    current = temp;
    int counter = 0;
    for (int i = 0; i < temp.ret_size(); i++)
    {
        for (int j = 0; j < temp.ret_size(); j++)
        {
            if (temp.ret_pos(i, j) == '0') // zliczam, ile pustych miejsce jeszcze na planszy -> ile mozliwych ruchow do wykonania
                counter++;
        }
    } // mam zrobione jakby drzewo. jeden rodzic, ma x gałęzi

    nodes = new tree_node[counter]; // tutaj tworzę odpowiednią ilość gałęzi
    int count = 0;
    int loop = 0;
    tree_node nnode(current); // tu mam jedno, modelowe odgałęzienie

    for (int i = 0; i < current.ret_size(); i++)
    {
        for (int j = 0; j < current.ret_size(); j++)
        {
            if (temp.ret_pos(i, j) == '0')
            {
                nnode.mov_board(temp, loop); // przekrztałcam to ''modelowe odgałęzienie''
                nnode.calc_val(width);
                nodes[count] = nnode; // do elementu w drzewie przypisuję to co jest w modelowym(mam przeciążony = też, żeby działało jak trzeba)
                count++;
            }
            loop++;
        }
    }

    int highest_node_number, cur_val, highest_val;
    highest_val = 0;
    highest_node_number = 0;
    for (int i = 0; i < counter; i++)
    {
        cur_val = nodes[i].ret_val();
        if (cur_val > highest_val)
        {
            highest_val = cur_val;
            highest_node_number = i;
        }
    }

    cur_board = nodes[highest_node_number].ret_board();
}
