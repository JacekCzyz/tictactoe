#include "board.cpp"

struct tree_node
{
    board move;
    int value;

    tree_node() {}
    tree_node(board current)
    {
        value = 0;
        int size = current.ret_size();
        board temp(size);
        move=temp;
    }
    ~tree_node() {}
    int ret_val() { return value; }
    board ret_board() { return move; }
    void operator=(tree_node newnode)
    {
        value = newnode.ret_val();
        move = newnode.ret_board();
    }
    void mov_board(board cur_board, int pos);
    void calc_val(int width);
};



class tree
{
private:
    board current;
    tree_node *nodes;
    int length;

public:
    tree() {}
    void ntree(board &cur_board, int width);
};